/*Java program in which a binary 2D array is taken as input.
 The traversal begins horizontally and keeps traversing in that direction for every zero encountered. 
 Every time a 1 is encountered, the traversal continues in the direction to the right of the current direction. 
 Traversal stops when the last row/column is reached and no 1 is encountered. */
import java.util.*;
public class Binary_dir{
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int row=sc.nextInt();
        int col=sc.nextInt();
        int arr[][]=new int [row][col];
        System.out.println("enter elements as either 0/1:");
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                arr[i][j]=sc.nextInt();
            }
        }

        int i=0,j=0;
        int d=0;
        while(true){
            d=(d+arr[i][j])%4;
            if(d==0){
                j++;
            }
            else if(d==1){
                i++;
            }
            else if(d==2){
                j--;
            }
            else if(d==3){
                i--;
            }

            if(i<0){
                i++;
                break;
            }
            else if(j<0){
                j++;
                break;
            }
            else if(i==arr.length){
                i--;
                break;
            }
            else if(j==arr[0].length){
                j--;
                break;
            }
        }
        System.out.println(i);
        System.out.println(j);
    }
}
