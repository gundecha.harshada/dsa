//Java program that traverses the even cols of a 2D array downwards and odd cols upward and prints its elements in that order. 
import java.util.*;
public class Up_and_down_array{
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int row=sc.nextInt();
        int col=sc.nextInt();
        int arr[][]=new int[row][col];
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                arr[i][j]=sc.nextInt();
            }
        }
        System.out.println("Output:");
        for(int j=0;j<col;j++){
            if(j%2==0){
                for(int i=0;i<row;i++){
                    System.out.println(arr[i][j]);
                }
            }
            else{
                for(int i=row-1;i>=0;i--){
                    System.out.println(arr[i][j]);
                }
            }
        }
        sc.close();
    }
}