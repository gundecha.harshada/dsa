import java.util.*;
public class Add_2_Arrays {
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        int size=sc.nextInt();
        int arr1[]=new int[size];
        int arr2[]=new int[size];
        System.out.println("Enter the elements of the first array:");
        for(int i=0;i<size;i++){
            arr1[i]=sc.nextInt();
        }
        System.out.println("Enter the elements of the second array:");
        for(int i=0;i<size;i++){
            arr2[i]=sc.nextInt();
        }
        int cy=0;
        int sum=0;
        int arr3[]=new int[size];
        for(int i=(size-1);i>=0;i--){
            if(cy>0){
                sum=arr1[i]+arr2[i]+cy;
                cy=0;
            }
            else{
                sum=arr1[i]+arr2[i];
            }

            if(sum>9 && i>0){
                int temp=sum;
                cy=temp/10;
                sum=sum%10;
            }
            arr3[i]=sum;
        }
        System.out.println("The sum of the two arrays is:");
        for(int i=0;i<size;i++){
            System.out.println(arr3[i]);
        }
        sc.close();
    }
}
